﻿using System;
using System.IO;

namespace CodeToBmp
{
    class Program
    {
        private static readonly byte[] Header = { 0x42, 0x4D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00};
        private static readonly byte[] LowerHeader = { 0x01, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20};
        
        static void Main(string[] args)
        {
            if(!File.Exists(args[0]))
                Console.WriteLine("Enter a valid file");

            int width = 1;
            int height = 1;
            
            if (args.Length > 1)
            {
                if (int.TryParse(args[1], out int desiredWidth))
                    width = desiredWidth;
                
                if (args.Length > 2)
                {
                    if (int.TryParse(args[2], out int desiredHeight))
                        height = desiredHeight;
                }
            }
            
            byte[] byteCode = File.ReadAllBytes(args[0]);
            int fillerBytes = 3 - byteCode.Length % 3;
            int newSize = byteCode.Length + fillerBytes;
            int lineCount = newSize / width;
            newSize += lineCount * 2;
            byte[] newByteArray = new byte[newSize];

            int offset = 0;
            for (int i = 0; i < lineCount; i++)
            {
                if (i * width + width > byteCode.Length)
                    Array.Copy(byteCode, i * width, newByteArray, i * width + offset, width);
                else
                    Array.Copy(byteCode, i * width, newByteArray, i * width + offset, width);
                
                newByteArray[i * width + offset + width + 1] = 0x00;
                newByteArray[i * width + offset + width + 2] = 0x00;

                offset += 2;
            }

            if (width * height < newByteArray.Length - lineCount * 2)
            {
                Console.WriteLine("Desired Size not large enough to encapsulate file.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            byte[] widthBytes = BitConverter.GetBytes(width);
            byte[] heightBytes = BitConverter.GetBytes(height);
            byte[] sizeBytes = BitConverter.GetBytes(newByteArray.Length + 55);
            Header[2] = sizeBytes[0];
            Header[3] = sizeBytes[1];
            Header[4] = sizeBytes[2];
            Header[5] = sizeBytes[3];

            using (FileStream fs = File.Create(Path.ChangeExtension(args[0], ".bmp")))
            {
                fs.Write(Header, 0, Header.Length);
                fs.Write(widthBytes, 0, 4);
                fs.Write(heightBytes, 0, 4);
                fs.Write(LowerHeader, 0, LowerHeader.Length);
                fs.Write(newByteArray, 0, newByteArray.Length);
            }
            
            Console.WriteLine($"Saved to {Path.ChangeExtension(args[0], ".bmp")}");
            Console.ReadLine();
        }
    }
}